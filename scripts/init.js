//Main Javascript function. Handles the program 
$(document).ready(() => {
  init_app();
});

//This will be used for the coursera API
courseraApi.callMethod({
  type: "GET_SESSION_CONFIGURATION",
  onSuccess: init_app
});

// Initialise the App
function init_app() {
  let mainApp = setInterval(() => {
      // Begin Game initialisation
      if (beginGame) {
        player = new Player();
        stack = new Stack();
        gameManager = new GameManager(player, stack);
        // Are we coming from tutorial ?
        if (beginTutorial) {
          gameManager.currentString = "tutorial";
        } else {
          gameManager.generateRandomString();
        }
        gameManager.assignRandomStringToList();
        // Run the code once
        clearInterval(mainApp);

        // Event Handlers----------------------------------------------------

        // Reset Current String and empty the Stack------------
        player.buttonReset.click(function () {
          stack = new Stack();
          gameManager.reset(stack);
          gameManager.assignValueToFeedback('Select one of the Actions');
          currentTutorialCount = 8;
          tutorial[currentTutorialCount].set();
        });

        // Generate a new Random String and clear the Stack------------
        player.buttonRandomString.click(function () {
          stack = new Stack();
          gameManager.newString(stack);
          gameManager.generateRandomString();
          gameManager.assignRandomStringToList();
          gameManager.assignValueToFeedback('Select one of the Actions');
          currentTutorialCount = 8;
          tutorial[currentTutorialCount].set();
        });


        // String Char Selection-----------------------
        player.listToPlayWith.click(function () {
          // clear all char selections 
          for (let index = 0; index < $(player.listToPlayWith).length; index++) {
            $(player.listToPlayWith[index]).removeClass('current-selection');
          }
          // add current selection
          $(this).addClass('current-selection');
          // Assign current selected char to the player variable 
          player.assignCurrentChar($(this).find('span').html());
          // If the current selection is not null
          if ($(this).find('span').html()) {
            // record the feedback
            gameManager.assignValueToFeedback('Your current selection is : ' + " ' " + $(this).find('span').html() + " '");
            // Selection animation
            gameManager.hrLine.addClass('active-feedback');
            setTimeout(function () {
              gameManager.hrLine.removeClass('active-feedback');
            }, 1000);
          } else {
            // record the feedback
            gameManager.assignValueToFeedback('Your current selection is : ' + " ' " + 'Empty' + " '");
            // selection animation
            gameManager.hrLine.addClass('active-feedback');
            setTimeout(function () {
              gameManager.hrLine.removeClass('active-feedback');
            }, 1000);
          }
        });

        // Top Button Click------------------------------
        player.buttonTop.click(function () {
          // Selection animation
          gameManager.hrLine.addClass('active-feedback');
          setTimeout(function () {
            gameManager.hrLine.removeClass('active-feedback');
          }, 1000);
          // If there is at least one element to Peek
          if (gameManager.stackInstance.size > 0) {
            let topValue = gameManager.stackInstance.peek();
            gameManager.assignValueToFeedback('The Stack top character is: ' + " ' " + topValue + " '");
          } else {
            gameManager.assignValueToFeedback('The Stack is empty, there is no top');
          }
        });

        // Pop Button Click------------------------------
        player.buttonPop.click(function () {
          // Selection animation
          gameManager.hrLine.addClass('active-feedback');
          setTimeout(function () {
            gameManager.hrLine.removeClass('active-feedback');
          }, 1000);
          // If there is at least one element to Pop
          if (gameManager.stackInstance.size > 0) {
            let popValue = gameManager.stackInstance.pop();
            gameManager.assignValueToFeedback('Character  ' + " ' " + popValue + " '" + ' popped from the Stack');
          } else {
            gameManager.assignValueToFeedback('The Stack is empty, nothing to pop');
          }
        });

        // Push Button Click----------------------------
        player.buttonPush.click(function () {
          // Selection animation
          gameManager.hrLine.addClass('active-feedback');
          setTimeout(function () {
            gameManager.hrLine.removeClass('active-feedback');
          }, 1000);
          // If the player selects a valid char and different from undefined or empty 
          if (player.currentCharSelected && player.currentCharSelected != "-" && player.currentCharSelected != "--") {
            // If the Stack has not reached its max size 
            if (gameManager.stackInstance.size < gameManager.stackInstance.maxSize) {
              // Push the element
              gameManager.stackInstance.push(player.currentCharSelected);
              // Update feedback
              gameManager.assignValueToFeedback('Character  ' + " ' " + player.currentCharSelected + " '" + ' pushed to the stack');
            } else {
              gameManager.assignValueToFeedback('The stack is currently full');
            }
            // Check when select emtpy char
          } else if (player.currentCharSelected == "--") {
            gameManager.assignValueToFeedback('You can not push empty characters');
            // Check when no char selected 
          } else {
            gameManager.assignValueToFeedback('Select one character to push');
          }
        });

        // IsEmpty Button Click----------------------------
        player.buttonIsEmpty.click(function () {
          // Selection animation
          gameManager.hrLine.addClass('active-feedback');
          setTimeout(function () {
            gameManager.hrLine.removeClass('active-feedback');
          }, 1000);
          // If the Stack is empty
          if (gameManager.stackInstance.size == 0) {
            gameManager.assignValueToFeedback('The stack is currently empty');
          } else {
            gameManager.assignValueToFeedback('The stack is Not empty');
          }
        });

        // Check answer yes------------------------------------
        player.buttonAnswer[0].addEventListener('click', (e) => {
          gameManager.acceptStringAnswer(e);
        });

        // Check answer no-------------------------------------
        player.buttonAnswer[1].addEventListener('click', (e) => {
          gameManager.rejectStringAnswer(e);
        });

        // Check answer cancel-------------------------------------
        player.buttonAnswer[2].addEventListener('click', () => {
          // undo the decision if submitting the answer
          gameManager.undoStringAnswer();
        });

        // Restart Interactive Tutorial
        $('#guide-button').click(function () {
          gameManager.currentString = "tutorial";
          gameManager.assignRandomStringToList();
          stack = new Stack();
          gameManager.reset(stack);
          gameManager.assignValueToFeedback('Select one of the Actions');
          currentTutorialCount = 11;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    1000);
};