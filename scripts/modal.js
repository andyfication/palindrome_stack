// General UI controls 
$(document).ready(() => {
  let initTutorial = setInterval(() => {
    if (showTutorial) {
      $(".modal").modal("show");
      clearInterval(initTutorial);
    }
  }, 500);
});