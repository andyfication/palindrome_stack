// Global variables
let currentTutorialCount = 0;
let beginGame = false;
let beginTutorial = false;
let comingFromTutorial = false;
let tutorial;
let gameManager;
let player;
let stack;

// Arrays with palindrome and non words 
let arrayOfStrings = ["ability", "able", "about", "act", "action", "add", "adult", "affect", "after", "agency", "agent", "age", "agree", "alone", "animal", "anyone", "apply", "area", "article", "around", "arrive", "artist", "avoid", "away", "baby", "back", "bad", "bag", "bed", "begin", "big", "bit", "blue", "body", "book", "building", "call", "camera", "career", "centre", "central", "challenge", "choice", "citizen", "coach", "cold", "common", "cost", "course", "create", "cup", "data", "daughter", "death", "decade", "decision", "defence", "describe", "despite", "detail", "dinner", "discuss", "director", "doctor", "dream", "door", "draw", "drive", "each", "edge", "energy", "example", "federal", "focus", "force", "form", "game", "girl", "glass", "goal", "group", "happy", "health", "heart", "history", "hotel", "idea", "impact", "include", "intro", "issue", "item", "job", "kitchen", "large", "leader", "letter", "machine", "main", "monitor", "market", "modern", "natural", "nation", "near", "name", "office", "offer", "open", "option", "owner", "patient", "problem", "process", "reason", "recent", "record", "school", "season", "science", "service", "similar", "simple", "sport", "subject", "test", "today", "under", "violence", "water", "week", "western", "wrong", "young", "yourself"];
let arrayOfPalindromes = ["bob", "pop", "wow", "adda", "anna", "noon", "civic", "level", "kayak", "madam", "minim", "radar", "refer", "rotor", "stats", "repaper", "rotator", "mom", "dad", "eye", "deed", "dvd", "gig", "sos", "hannah"];

$(document).ready(() => {

  // Tutorial Object, has text to display and a function set to change the state of the content
  tutorial = [{
      text: "Welcome, I'll be your task manager and guide <button class = 'button-next'> Next</button> <button class = 'button-skip'>Skip</button>",
      index: 0,
      set: function () {
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Here you can find your stack interaction panel <button class = 'button-next'>Next</button>",
      index: 1,
      set: function () {
        $('#interaction-grid-transparent').css('display', 'none');
        $('#interaction-grid').addClass('highlightBorder');
        $('#interaction-grid').addClass('non-active');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Here you can select your string characters <button class = 'button-next'>Next</button>",
      index: 2,
      set: function () {
        $('#interaction-grid').removeClass('highlightBorder');
        $('#interactive-string').addClass('highlightBorder');
        $('#interactive-buttons-stack-transparent').css('display', 'block');
        $('#interactive-buttons-reset-transparent').css('display', 'block');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Here you can interact with your stack and check feedback<button class = 'button-next'>Next</button>",
      index: 3,
      set: function () {
        $('#interactive-string').removeClass('highlightBorder');
        $('#interactive-string-transparent').css('display', 'block');
        $('#interactive-buttons-stack-transparent').css('display', 'none');
        $('#interactive-buttons-stack').addClass('highlightBorder');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Here you can reset your stack or generate a new string<button class = 'button-next'>Next</button>",
      index: 4,
      set: function () {
        $('#interactive-buttons-stack').removeClass('highlightBorder');
        $('#interactive-buttons-stack-transparent').css('display', 'block');
        $('#interactive-buttons-reset-transparent').css('display', 'none');
        $('#interactive-buttons-reset').addClass('highlightBorder');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Here you can view your stack and check the string output<button class = 'button-next'>Next</button>",
      index: 5,
      set: function () {
        $('#interactive-buttons-reset').removeClass('highlightBorder');
        $('#interactive-buttons-reset-transparent').css('display', 'block');
        $('#stack-grid-transparent').css('display', 'none');
        $('#stack-grid').addClass('highlightBorder');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Finally you can use the [Help] button to review this information <button class = 'button-next'>Next</button>",
      index: 6,
      set: function () {
        $('#stack-grid-transparent').css('display', 'block');
        $('#stack-grid').removeClass('highlightBorder');
        $('#help').addClass('highlightBorder');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Let me guide your first attempt<button class = 'button-next'>Ok</button> <button class = 'button-skip'>Skip</button>",
      index: 7,
      set: function () {
        $('#help').removeClass('highlightBorder');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Use the stack to check whether a string is a palindrome <button class = 'button-next'>Begin</button> ",
      index: 8,
      set: function () {
        $('#interactive-buttons-stack-transparent').css('display', 'block');
        let arrayElement = $('.stack-array-element');
        arrayElement.removeClass('active');
        arrayElement.addClass('non-active');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Submit your answer when you have the solution<button class = 'button-next'>Submit</button> ",
      index: 9,
      set: function () {
        if (!comingFromTutorial) {
          console.log('Not From tutorial');
          let arrayElement = $('.stack-array-element');
          arrayElement.removeClass('non-active');
          arrayElement.addClass('active');
          $('#pop-button').removeClass('highlightBorder');
          $('#top-button').removeClass('highlightBorder');
          $('#push-button').removeClass('highlightBorder');
          $('#empty-button').removeClass('highlightBorder');
          $('#pop-button').removeClass('non-active');
          $('#top-button').removeClass('non-active');
          $('#push-button').removeClass('non-active');
          $('#empty-button').removeClass('non-active');
          $('#interactive-buttons-reset').removeClass('highlightBorder');
          $('#stack-grid').removeClass('highlightBorder');
          $('#stack-grid-transparent').css('display', 'none');
          $('#interaction-grid-transparent').css('display', 'none');
          $('#interactive-string-transparent').css('display', 'none');
          $('#interactive-buttons-stack-transparent').css('display', 'none');
          $('#interactive-buttons-reset-transparent').css('display', 'none');
          $('#interaction-grid').removeClass('non-active');
          $('#suggestion').html(this.text);
          $('#reset-button').removeClass('non-active');
          $('#random-button').removeClass('non-active');
          $('.answer').css('display', 'none');
          // We can start the Game
          beginGame = true;
          beginTutorial = false;
          comingFromTutorial = false;
        } else {
          console.log('From tutorial');
          $('#suggestion').html("Submit your answer now <button class = 'button-next'>Submit</button> ");
          $('#pop-button').removeClass('highlightBorder');
          $('#pop-button').removeClass('active');
          $('#reset-button').addClass('non-active');
          $('#random-button').addClass('non-active');
        }
      }
    },
    {
      text: "Is the given string a palindrome ?",
      index: 10,
      set: function () {
        $('#interactive-buttons-stack-transparent').css('display', 'block');
        $('#interactive-buttons-reset-transparent').css('display', 'block');
        if (!comingFromTutorial) {
          console.log('Nope not from tutorial');
          $('.answer').css('display', 'inline-block');
        } else {
          $('.answer').css('display', 'inline-block');
          $($('.answer')[2]).css('display', 'none');
          comingFromTutorial = false;
        }
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Let's first check if the stack is empty",
      index: 11,
      set: function () {
        beginGame = true;
        beginTutorial = true;
        let arrayElement = $('.stack-array-element');
        arrayElement.addClass('non-active');
        arrayElement.removeClass('active');
        $('#interaction-grid-transparent').css('display', 'none');
        $('#interactive-string-transparent').css('display', 'none');
        $('#interactive-buttons-stack-transparent').css('display', 'none');
        $('#interactive-buttons-reset-transparent').css('display', 'none');
        $('#stack-grid-transparent').css('display', 'none');
        $('#top-button').addClass('non-active');
        $('#pop-button').addClass('non-active');
        $('#push-button').addClass('non-active');
        $('#empty-button').addClass('non-active');
        $('#reset-button').addClass('non-active');
        $('#random-button').addClass('non-active');
        $('#empty-button').addClass('active');
        $('#empty-button').addClass('highlightBorder');
        $('#suggestion').html(this.text);
        $('#empty-button').one('click', function () {
          $('#button-feed-container').addClass('highlightFeedback');
          currentTutorialCount = 12;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, now select the first string character",
      index: 12,
      set: function () {
        $('#empty-button').removeClass('active');
        $('#empty-button').removeClass('highlightBorder');
        let arrayElement = $('.stack-array-element');
        arrayElement.addClass('non-active');
        arrayElement.removeClass('active');
        $(arrayElement[0]).removeClass('non-active');
        $(arrayElement[0]).addClass('active');
        $('#interactive-string').addClass('highlightBorder');
        $('#suggestion').html(this.text);
        $(arrayElement[0]).one('click', function () {
          currentTutorialCount = 13;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, now push your first character to the stack",
      index: 13,
      set: function () {
        $('#button-feed-container').removeClass('highlightFeedback');
        $('#push-button').addClass('active');
        $('#push-button').addClass('highlightBorder');
        let arrayElement = $('.stack-array-element');
        arrayElement.addClass('non-active');
        arrayElement.removeClass('active');
        $('#interactive-string').removeClass('highlightBorder');
        $('#suggestion').html(this.text);
        $('#push-button').one('click', function () {
          currentTutorialCount = 14;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, now let's check the top of our stack",
      index: 14,
      set: function () {
        $('#push-button').removeClass('active');
        $('#push-button').removeClass('highlightBorder');
        $('#top-button').addClass('active');
        $('#top-button').addClass('highlightBorder');
        $('#suggestion').html(this.text);
        $('#top-button').one('click', function () {
          currentTutorialCount = 15;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, now select the next string character",
      index: 15,
      set: function () {
        $('#top-button').removeClass('active');
        $('#top-button').removeClass('highlightBorder');
        let arrayElement = $('.stack-array-element');
        arrayElement.addClass('non-active');
        arrayElement.removeClass('active');
        $(arrayElement[1]).removeClass('non-active');
        $(arrayElement[1]).addClass('active');
        $('#interactive-string').addClass('highlightBorder');
        $('#suggestion').html(this.text);
        $(arrayElement[1]).one('click', function () {
          currentTutorialCount = 16;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, now push the character to the stack",
      index: 16,
      set: function () {
        $('#interactive-string').removeClass('highlightBorder');
        let arrayElement = $('.stack-array-element');
        arrayElement.addClass('non-active');
        arrayElement.removeClass('active');
        $('#push-button').addClass('highlightBorder');
        $('#push-button').addClass('active');
        $('#suggestion').html(this.text);
        $('#push-button').one('click', function () {
          currentTutorialCount = 17;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, now select the next string character",
      index: 17,
      set: function () {
        $('#push-button').removeClass('highlightBorder');
        $('#push-button').removeClass('active');
        $('#top-button').removeClass('highlightBorder');
        $('#top-button').removeClass('active');
        let arrayElement = $('.stack-array-element');
        arrayElement.addClass('non-active');
        arrayElement.removeClass('active');
        $(arrayElement[2]).removeClass('non-active');
        $(arrayElement[2]).addClass('active');
        $('#interactive-string').addClass('highlightBorder');
        $('#suggestion').html(this.text);
        $(arrayElement[2]).one('click', function () {
          currentTutorialCount = 18;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, now push the character to the stack",
      index: 18,
      set: function () {
        $('#interactive-string').removeClass('highlightBorder');
        let arrayElement = $('.stack-array-element');
        arrayElement.addClass('non-active');
        arrayElement.removeClass('active');
        $('#push-button').addClass('highlightBorder');
        $('#push-button').addClass('active');
        $('#suggestion').html(this.text);
        $('#push-button').one('click', function () {
          currentTutorialCount = 19;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, now select the next string character",
      index: 19,
      set: function () {
        $('#push-button').removeClass('highlightBorder');
        $('#push-button').removeClass('active');
        $('#top-button').removeClass('highlightBorder');
        $('#top-button').removeClass('active');
        let arrayElement = $('.stack-array-element');
        arrayElement.addClass('non-active');
        arrayElement.removeClass('active');
        $(arrayElement[3]).removeClass('non-active');
        $(arrayElement[3]).addClass('active');
        $('#interactive-string').addClass('highlightBorder');
        $('#suggestion').html(this.text);
        $(arrayElement[3]).one('click', function () {
          currentTutorialCount = 20;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, now push the character to the stack",
      index: 20,
      set: function () {
        $('#interactive-string').removeClass('highlightBorder');
        let arrayElement = $('.stack-array-element');
        arrayElement.addClass('non-active');
        arrayElement.removeClass('active');
        $('#push-button').addClass('highlightBorder');
        $('#push-button').addClass('active');
        $('#suggestion').html(this.text);
        $('#push-button').one('click', function () {
          currentTutorialCount = 21;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, now select the next string character",
      index: 21,
      set: function () {
        $('#push-button').removeClass('highlightBorder');
        $('#push-button').removeClass('active');
        $('#top-button').removeClass('highlightBorder');
        $('#top-button').removeClass('active');
        let arrayElement = $('.stack-array-element');
        arrayElement.addClass('non-active');
        arrayElement.removeClass('active');
        $(arrayElement[4]).removeClass('non-active');
        $(arrayElement[4]).addClass('active');
        $('#interactive-string').addClass('highlightBorder');
        $('#suggestion').html(this.text);
        $(arrayElement[4]).one('click', function () {
          currentTutorialCount = 22;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, now push the character to the stack",
      index: 22,
      set: function () {
        $('#interactive-string').removeClass('highlightBorder');
        let arrayElement = $('.stack-array-element');
        arrayElement.addClass('non-active');
        arrayElement.removeClass('active');
        $('#push-button').addClass('highlightBorder');
        $('#push-button').addClass('active');
        $('#suggestion').html(this.text);
        $('#push-button').one('click', function () {
          currentTutorialCount = 23;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, now select the next string character",
      index: 23,
      set: function () {
        $('#push-button').removeClass('highlightBorder');
        $('#push-button').removeClass('active');
        $('#top-button').removeClass('highlightBorder');
        $('#top-button').removeClass('active');
        let arrayElement = $('.stack-array-element');
        arrayElement.addClass('non-active');
        arrayElement.removeClass('active');
        $(arrayElement[5]).removeClass('non-active');
        $(arrayElement[5]).addClass('active');
        $('#interactive-string').addClass('highlightBorder');
        $('#suggestion').html(this.text);
        $(arrayElement[5]).one('click', function () {
          currentTutorialCount = 24;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, now push the character to the stack",
      index: 24,
      set: function () {
        $('#interactive-string').removeClass('highlightBorder');
        let arrayElement = $('.stack-array-element');
        arrayElement.addClass('non-active');
        arrayElement.removeClass('active');
        $('#push-button').addClass('highlightBorder');
        $('#push-button').addClass('active');
        $('#suggestion').html(this.text);
        $('#push-button').one('click', function () {
          currentTutorialCount = 25;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, now select the next string character",
      index: 25,
      set: function () {
        $('#push-button').removeClass('highlightBorder');
        $('#push-button').removeClass('active');
        $('#top-button').removeClass('highlightBorder');
        $('#top-button').removeClass('active');
        let arrayElement = $('.stack-array-element');
        arrayElement.addClass('non-active');
        arrayElement.removeClass('active');
        $(arrayElement[6]).removeClass('non-active');
        $(arrayElement[6]).addClass('active');
        $('#interactive-string').addClass('highlightBorder');
        $('#suggestion').html(this.text);
        $(arrayElement[6]).one('click', function () {
          currentTutorialCount = 26;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, now push the character to the stack",
      index: 26,
      set: function () {
        $('#interactive-string').removeClass('highlightBorder');
        let arrayElement = $('.stack-array-element');
        arrayElement.addClass('non-active');
        arrayElement.removeClass('active');
        $('#push-button').addClass('highlightBorder');
        $('#push-button').addClass('active');
        $('#suggestion').html(this.text);
        $('#push-button').one('click', function () {
          currentTutorialCount = 27;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, now select the next string character",
      index: 27,
      set: function () {
        $('#push-button').removeClass('highlightBorder');
        $('#push-button').removeClass('active');
        $('#top-button').removeClass('highlightBorder');
        $('#top-button').removeClass('active');
        let arrayElement = $('.stack-array-element');
        arrayElement.addClass('non-active');
        arrayElement.removeClass('active');
        $(arrayElement[7]).removeClass('non-active');
        $(arrayElement[7]).addClass('active');
        $('#interactive-string').addClass('highlightBorder');
        $('#suggestion').html(this.text);
        $(arrayElement[7]).one('click', function () {
          currentTutorialCount = 28;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, now push the character to the stack",
      index: 28,
      set: function () {
        $('#interactive-string').removeClass('highlightBorder');
        let arrayElement = $('.stack-array-element');
        arrayElement.addClass('non-active');
        arrayElement.removeClass('active');
        $('#push-button').addClass('highlightBorder');
        $('#push-button').addClass('active');
        $('#suggestion').html(this.text);
        $('#push-button').one('click', function () {
          currentTutorialCount = 29;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, let's check if the stack is empty now",
      index: 29,
      set: function () {
        $('#interactive-string').removeClass('highlightBorder');
        let arrayElement = $('.stack-array-element');
        arrayElement.addClass('non-active');
        arrayElement.removeClass('active');
        $('#empty-button').addClass('highlightBorder');
        $('#empty-button').addClass('active');
        $('#push-button').removeClass('highlightBorder');
        $('#push-button').removeClass('active');
        $('#suggestion').html(this.text);
        $('#empty-button').one('click', function () {
          currentTutorialCount = 30;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, let's check the top of the stack again",
      index: 30,
      set: function () {
        $('#interactive-string').removeClass('highlightBorder');
        $('#empty-button').removeClass('highlightBorder');
        $('#empty-button').removeClass('active');
        $('#top-button').addClass('highlightBorder');
        $('#top-button').addClass('active');
        $('#suggestion').html(this.text);
        $('#top-button').one('click', function () {
          currentTutorialCount = 31;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, let's pop our first value from the stack now",
      index: 31,
      set: function () {
        $('#interactive-string').removeClass('highlightBorder');
        $('#top-button').removeClass('highlightBorder');
        $('#top-button').removeClass('active');
        $('#pop-button').addClass('highlightBorder');
        $('#pop-button').addClass('active');
        $('#suggestion').html(this.text);
        $('#pop-button').one('click', function () {
          currentTutorialCount = 32;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, keep popping the stack",
      index: 32,
      set: function () {
        $('#top-button').removeClass('highlightBorder');
        $('#top-button').removeClass('active');
        $('#pop-button').addClass('highlightBorder');
        $('#pop-button').addClass('active');
        $('#suggestion').html(this.text);
        $('#pop-button').one('click', function () {
          currentTutorialCount = 33;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, keep popping the stack",
      index: 33,
      set: function () {
        $('#top-button').removeClass('highlightBorder');
        $('#top-button').removeClass('active');
        $('#pop-button').addClass('highlightBorder');
        $('#pop-button').addClass('active');
        $('#suggestion').html(this.text);
        $('#pop-button').one('click', function () {
          currentTutorialCount = 34;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, keep popping the stack",
      index: 34,
      set: function () {
        $('#top-button').removeClass('highlightBorder');
        $('#top-button').removeClass('active');
        $('#pop-button').addClass('highlightBorder');
        $('#pop-button').addClass('active');
        $('#suggestion').html(this.text);
        $('#pop-button').one('click', function () {
          currentTutorialCount = 35;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, keep popping the stack",
      index: 35,
      set: function () {
        $('#top-button').removeClass('highlightBorder');
        $('#top-button').removeClass('active');
        $('#pop-button').addClass('highlightBorder');
        $('#pop-button').addClass('active');
        $('#suggestion').html(this.text);
        $('#pop-button').one('click', function () {
          currentTutorialCount = 36;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, keep popping the stack",
      index: 36,
      set: function () {
        $('#top-button').removeClass('highlightBorder');
        $('#top-button').removeClass('active');
        $('#pop-button').addClass('highlightBorder');
        $('#pop-button').addClass('active');
        $('#suggestion').html(this.text);
        $('#pop-button').one('click', function () {
          currentTutorialCount = 37;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, keep popping the stack",
      index: 37,
      set: function () {
        $('#top-button').removeClass('highlightBorder');
        $('#top-button').removeClass('active');
        $('#pop-button').addClass('highlightBorder');
        $('#pop-button').addClass('active');
        $('#suggestion').html(this.text);
        $('#pop-button').one('click', function () {
          currentTutorialCount = 38;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Great, keep popping the stack",
      index: 38,
      set: function () {
        $('#top-button').removeClass('highlightBorder');
        $('#top-button').removeClass('active');
        $('#pop-button').addClass('highlightBorder');
        $('#pop-button').addClass('active');
        $('#suggestion').html(this.text);
        $('#pop-button').one('click', function () {
          comingFromTutorial = true;
          currentTutorialCount = 9;
          tutorial[currentTutorialCount].set();
        });
      }
    }
  ];

  // Setting the first tutorial task
  tutorial[currentTutorialCount].set();

  // Increment tutorial task on button click 
  $('#suggestion').on('click', '.button-next', () => {
    // if click on tutorial begin tutorial 
    if (currentTutorialCount == 7) {
      currentTutorialCount = 11;
      tutorial[currentTutorialCount].set();
    } else if (currentTutorialCount == 8) {
      $('#button-feed').html('Select one of the Actions');
      // Last tutorial task before the game 
      currentTutorialCount += 1;
      tutorial[currentTutorialCount].set();
    } else {
      // Last tutorial task before the game 
      currentTutorialCount += 1;
      tutorial[currentTutorialCount].set();
    }

  });
  //Jump the tutorial section 
  $('#suggestion').on('click', '.button-skip', () => {
    // Skip the intro tutorial
    if (currentTutorialCount == 0) {
      currentTutorialCount = 7;
      tutorial[currentTutorialCount].set();
      // Skip the guided tutorial 
    } else if (currentTutorialCount == 7) {
      currentTutorialCount = 8;
      tutorial[currentTutorialCount].set();
    }
  });
  // Toggle the Graph Legenda on help button click 
  $('#help').on('click', () => {
    $(".modal-title").html('Palindrome Stack');
    $(".modal-body").html('<h3 style="margin-top:15px">Overview</h3><p> Use the <strong>Stack</strong> data structure to determine if a given string <br /> is a <strong>Palindrome</strong> word or not. Rememebr that elements <br /> that enter the <strong>Stack</strong> first are the last to leave.<br /> If you execute all the steps correctly, you will get the reverse order of the <strong>String</strong> characters pushed to the <strong>Stack</strong>. <br /> At this point, if the two <strong>Strings</strong> match, the word is a palindrome.</p> <h3>Operations</h3> <p style = "font-size:20px; margin-top: 20px; font-weight: bold"> Top: <span style = "font-size:16px; font-weight: normal">View the top element of the Stack</span></p>  <p style = "font-size:20px; font-weight: bold"> Pop: <span style = "font-size:16px; font-weight: normal">Remove and store the top element from the Stack</span></p>  <p style = "font-size:20px; font-weight: bold"> Push: <span style = "font-size:16px; font-weight: normal">Add the element at the top of the Stack</span></p>  <p style = "font-size:20px; font-weight: bold"> Is Empty: <span style = "font-size:16px; font-weight: normal">Check if the Stack contains any elements</span></p>');
    $(".modal").modal("show");
  });

  // Activete interactive guide button
  $('#closeModal').one('click', () => {
    $('#guide-button').css('display', 'block');
  });

  // Activete interactive guide button
  $('#close-x').one('click', () => {
    $('#guide-button').css('display', 'block');
  });
});