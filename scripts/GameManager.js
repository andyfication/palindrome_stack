// class GameManager which handles the game 
class GameManager {

  constructor(player, stack) {
    this.currentString = "";
    this.isPalindrome = "false";
    this.playerInstance = player;
    this.stackInstance = stack;
    this.shortFeedback = $('#button-feed');
    this.hrLine = $('#hr-feed');
  }
  // Reset the Game Manager
  reset(stack) {
    this.playerInstance.reset();
    this.stackInstance = stack;
  }
  // Reset Game Manager when generating a new random string 
  newString(stack) {
    this.playerInstance.reset();
    this.stackInstance = stack;
    this.currentString = "";
    this.isPalindrome = "false";
  }

  // Generate and Store the current String
  generateRandomString() {
    let randomChoice = Math.random();
    // Choose randomly between palindrom and non-palindrom array
    if (randomChoice > 0.5) {
      let randomIndex = Math.floor(Math.random() * arrayOfPalindromes.length);
      this.isPalindrome = "true";
      this.currentString = arrayOfPalindromes[randomIndex];
    } else {
      let randomIndex = Math.floor(Math.random() * arrayOfStrings.length);
      this.isPalindrome = "false";
      this.currentString = arrayOfStrings[randomIndex];
    }
  }

  // Assigns the random generated string to the list
  assignRandomStringToList() {
    // get array from the current string
    let arrayFromString = this.currentString.split("");
    // Fill in list with current array elements and check for blank characters
    for (let index = 0; index < $(this.playerInstance.listToPlayWith).length; index++) {
      if (arrayFromString[index]) {
        $(this.playerInstance.listToPlayWith[index]).find("span").html(arrayFromString[index]);
      } else {
        $(this.playerInstance.listToPlayWith[index]).find("span").html(" ");
      }
    }
  }

  // Assign value to short feedback 
  assignValueToFeedback(text) {
    this.shortFeedback.html(text);
  }

  // Function that checks on button 'yes'
  acceptStringAnswer(e) {
    $('#interactive-buttons-reset-transparent').css('display', 'none');
    $('#interaction-grid').removeClass('non-active');
    $('#reset-button').removeClass('non-active');
    $('#random-button').removeClass('non-active');
    // The string is a palindrome
    if (e.target.value == this.isPalindrome) {
      // The stack was well managed 
      if (this.currentString == stack.getStringFromAnswerList()) {
        $('#suggestion').html('Well done, the current string is a palindrome');
        $('.answer').css('display', 'none');
        $('#interactive-buttons-reset').addClass('highlightBorder');
      } else {
        $('#suggestion').html('The string is a palindrome but you need to use the stack to confirm it');
        $('.answer').css('display', 'none');
        $('#stack-grid').addClass('highlightBorder');
        $('#interactive-buttons-reset').addClass('highlightBorder');
      }
    } else {
      $('#suggestion').html('Not really, the string is not a palindrome');
      $('.answer').css('display', 'none');
      $('#interactive-buttons-reset').addClass('highlightBorder');
    }
  }
  // Function that checks on button 'no'
  rejectStringAnswer(e) {
    // The string is not a palindrome
    $('#interactive-buttons-reset-transparent').css('display', 'none');
    $('#interaction-grid').removeClass('non-active');
    $('#reset-button').removeClass('non-active');
    $('#random-button').removeClass('non-active');
    if (e.target.value == this.isPalindrome) {
      // The stack was well managed 
      if (this.currentString == stack.getReverseStringFromAnswerList()) {
        $('#suggestion').html('Well done, the current string is not a palindrome');
        $('.answer').css('display', 'none');
        $('#interactive-buttons-reset').addClass('highlightBorder');
      } else {
        $('#suggestion').html('The string is not a palindrome but you need to use the stack to confirm it');
        $('.answer').css('display', 'none');
        $('#stack-grid').addClass('highlightBorder');
        $('#interactive-buttons-reset').addClass('highlightBorder');
      }
    } else {
      $('#suggestion').html('Not really, the string is a palindrome');
      $('.answer').css('display', 'none');
      $('#interactive-buttons-reset').addClass('highlightBorder');
    }
  }

  // Undo the answer choice and go back to the previuos task 
  undoStringAnswer() {
    currentTutorialCount = 9;
    tutorial[currentTutorialCount].set();
  }
}