// Variables
var mainAimationFinished = false;
var showTutorial = false;
var $mainContainer;


$(document).ready(function () {
  var $introContainer = $('#introContainer');
  $mainContainer = $('#main-container-grid');
  var $beginButton = $('#begin');
  var $introImageActive = $('#introImageActive');
  // Fade in main container 4 seconds 
  setTimeout(function () {
    $introContainer.fadeIn(4000);
  }, 1000)
  // Start button begin animation
  $beginButton.on('click', function () {
    $introImageActive.fadeIn(3000, function () {
      $introContainer.fadeOut(2000, function () {
        $mainContainer.css('display', 'grid');
        $mainContainer.fadeIn(3000, function () {
          // End of animation 
          mainAimationFinished = true;
        });
      });
    });
  })
})
// Wait for the end animation before enabling the plugin container
var waitingForEndAnimation = setInterval(function () {
  if (mainAimationFinished) {
    $mainContainer.css('visibility', 'visible');
    showTutorial = true;
    clearInterval(waitingForEndAnimation);
  }
}, 1000);