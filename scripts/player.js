// Class Player which stores all the interactable items
class Player {

  constructor() {
    // List of player button interaction 
    this.listToPlayWith = $('.stack-array-element');
    this.buttonTop = $('#top-button');
    this.buttonPop = $('#pop-button');
    this.buttonPush = $('#push-button');
    this.buttonIsEmpty = $('#empty-button');
    this.buttonReset = $('#reset-button');
    this.buttonRandomString = $('#random-button');
    this.buttonAnswer = $('.answer');
    // Current list char selected
    this.currentCharSelected = "-";
  }
  // Reset the char selection
  reset() {
    this.currentCharSelected = "-";
  }

  // Assign current selected character 
  assignCurrentChar(char) {
    // If empty selection 
    if (!char)
      // Assign this symbol 
      this.currentCharSelected = "--";
    else
      // Valid char
      this.currentCharSelected = char;
  }
}