//Class Stack which handles the data structure and dynamic html list 
class Stack {

  constructor() {
    this.structure = [];
    this.top = null;
    this.size = 0;
    this.maxSize = 8;
    this.stackListReference = document.getElementById('stack-list');
    this.listAnswerReference = $('#stack-answer-container');
    this.initialPosition = 380;
    this.aswerIndex = 0;
    this.clearLists();
  }

  // Push a value to the Stack 
  push(value) {
    let newStackNode = new StackNode(value, this.top);
    this.top = newStackNode;
    this.size++;
    this.structure.push(this.top.value);
    // Add node to Dynamic visual list 
    // If we have not reached the maxSize
    if (this.stackListReference.getElementsByTagName("li").length < this.maxSize) {
      let valToAdd = value;
      // Create li element with span node
      let elementNode = document.createElement('li');
      elementNode.className = 'stack-node';
      let spanNode = document.createElement('span');
      spanNode.className = 'stack-val not-visible';
      let elementText = document.createTextNode(valToAdd);
      spanNode.append(elementText);
      elementNode.append(spanNode);
      this.stackListReference.insertBefore(elementNode, this.stackListReference.childNodes[0]);
      // Move the list up
      this.initialPosition -= 38;
      this.stackListReference.style.top = this.initialPosition + 'px';
      // Update the li text visibility
      for (let index = 0; index < this.stackListReference.getElementsByTagName("li").length; index++) {
        this.stackListReference.childNodes[index].firstChild.className = 'stack-val not-visible';
      }
    }
    // If there is at least one element hide the emtpy text
    if (this.size > 0 && this.size != this.maxSize) {
      $('#empty-stack-text').html("Loading Stack");
    } // If full display full 
    else if (this.size == this.maxSize) {
      $('#empty-stack-text').html("Full Stack");
    }
  }

  // Pop a value from the Stack 
  pop() {
    let value = null;
    if (this.top) {
      value = this.top.value;
      this.top = this.top.nextNode;
      this.size--;
      this.structure.pop();
      // Remove node to Dynamic visual list
      if (this.stackListReference.getElementsByTagName("li").length != 0) {
        this.stackListReference.removeChild(this.stackListReference.childNodes[0]);
        this.initialPosition += 38;
        this.stackListReference.style.top = this.initialPosition + 'px';
      }
      // If there are no elements display empty text 
      if (this.size < 1) {
        $('#empty-stack-text').html("Empty Stack");
      } else
      // display load text
      {
        $('#empty-stack-text').html("Loading Stack");
      }
      // Add value to asnwer list 
      this.addPoppedValueToAnswerList(value)
    }
    return value;
  }

  // Peek the top value without removing it from the Stack 
  peek() {
    let value = null;
    if (this.top) {
      value = this.top.value
      // Show Top element on the Dynamic List
      if (this.stackListReference.childNodes[0].firstChild != null)
        this.stackListReference.childNodes[0].firstChild.classList.remove('not-visible');
    }
    return value;
  }

  // Return stack elements as a string of values 
  print() {
    let elementsString = "";
    let elementTempArray = [];
    for (let index = 0; index < this.structure.length; index++) {
      elementTempArray.push("'" + this.structure[index] + "'");
    }
    elementsString = elementTempArray.toString();
    return elementsString;
  }

  // Add popped value to the answer list
  addPoppedValueToAnswerList(value) {
    $(this.listAnswerReference.find('span')[this.aswerIndex]).html(value);
    // Can add maximum 8 characters 
    if (this.aswerIndex < this.maxSize) {
      this.aswerIndex = (this.aswerIndex + 1);
    }
  }

  //Clear lists and reset feedback and highlights elements
  clearLists() {
    // Clear answer list 
    for (let index = 0; index < this.maxSize; index++) {
      $(this.listAnswerReference.find('span')[index]).html("");
    }
    // Clear visual stack 
    let index = 0;
    while (index < this.stackListReference.getElementsByTagName("li").length) {
      this.stackListReference.removeChild(this.stackListReference.childNodes[0]);
    }
    // Reset feedback
    $('#empty-stack-text').html("Empty Stack");
    //Reset highlight colours
    for (let index = 0; index < this.maxSize; index++) {
      $('.stack-array-element').removeClass('current-selection');
    }

  }

  // Create string from the Answer list and return the value 
  getStringFromAnswerList() {
    let stringAnswer = "";
    for (let index = 0; index < this.maxSize; index++) {
      stringAnswer += $(this.listAnswerReference.find('span')[index]).html();
    }
    return stringAnswer;
  }

  // Create a reverse string from the Answer list and return the value. Used for non palindrome words
  getReverseStringFromAnswerList() {
    let stringAnswer = "";
    for (let index = 0; index < this.maxSize; index++) {
      stringAnswer += $(this.listAnswerReference.find('span')[index]).html();
    }
    // Reversing the string using built in array functions 
    let splitString = stringAnswer.split("");
    let reverseArray = splitString.reverse();
    let joinArray = reverseArray.join("");
    stringAnswer = joinArray;
    return stringAnswer;

  }
}