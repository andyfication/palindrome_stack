This is my repo for the Palindrome Stack project.


I worked with Goldsmiths, University of London and Coursera to develop some online interactive learning tools. The Palindrome Stack is among these.

The Palindrome Stack is an interactive simulation made in HTML5,CSS3 and javaScript.

The plugin shows step by step how to use stack data structures to check whether a given word is palindrome or not.
